//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKWriteResourceManagerStateBehavior_h
#define pqSMTKWriteResourceManagerStateBehavior_h

#include "pqReaction.h"

#include <QObject>

class pqServer;
class pqSMTKWrapper;

/// A reaction for writing a resource manager's state to disk.
class pqWriteResourceManagerStateReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqWriteResourceManagerStateReaction(QAction* parent);

  static void saveResourceManagerState();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { pqWriteResourceManagerStateReaction::saveResourceManagerState(); }

private:
  Q_DISABLE_COPY(pqWriteResourceManagerStateReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqSMTKWriteResourceManagerStateBehavior
  : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqSMTKWriteResourceManagerStateBehavior* instance(QObject* parent = nullptr);
  ~pqSMTKWriteResourceManagerStateBehavior() override;

protected:
  pqSMTKWriteResourceManagerStateBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqSMTKWriteResourceManagerStateBehavior);
};

#endif
