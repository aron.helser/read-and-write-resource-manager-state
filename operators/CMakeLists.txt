# Set source files
set(srcs
  Registrar.cxx
)

# Set header files
set(headers
  Registrar.h
)

set(rmOperators
  ReadResourceManagerState
  WriteResourceManagerState
  )

foreach (operator ${rmOperators})
  smtk_encode_file("${CMAKE_CURRENT_SOURCE_DIR}/${operator}.sbt"
  TARGET_OUTPUT targetName)
  list(APPEND srcs ${operator}.cxx)
  list(APPEND headers ${operator}.h)
  list(APPEND _rmDependencies ${targetName})
endforeach()

# Declare the library
add_library(smtkReadWriteResourceManagerState
 ${srcs}
)
add_dependencies(smtkReadWriteResourceManagerState ${_rmDependencies})

target_link_libraries(smtkReadWriteResourceManagerState
  PUBLIC
    smtkCore
    Boost::boost
    Boost::filesystem
)

target_include_directories(smtkReadWriteResourceManagerState PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}>
)

generate_export_header(smtkReadWriteResourceManagerState EXPORT_FILE_NAME Exports.h)

smtk_get_kit_name(name dir_prefix)

# Install the header files
install(
  FILES
    ${headers}
    ${CMAKE_CURRENT_BINARY_DIR}/Exports.h
  DESTINATION
  ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})

# Install the library and exports
install(
  TARGETS smtkReadWriteResourceManagerState
  EXPORT  ${PROJECT_NAME}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})
